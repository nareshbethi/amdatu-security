/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.backend.file;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.crypto.Mac;
import javax.crypto.SecretKey;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.Account.State;

/**
 * Helper class to (de)serialize {@link Account}s.
 */
class IOUtils {

    public static class HmacInputStream extends FilterInputStream {
        private final Mac m_mac;

        public HmacInputStream(InputStream in, SecretKey key) throws GeneralSecurityException {
            super(in);

            m_mac = Mac.getInstance(key.getAlgorithm());
            m_mac.init(key);
        }

        public byte[] getMAC() {
            return m_mac.doFinal();
        }

        @Override
        public boolean markSupported() {
            return false;
        }

        @Override
        public int read() throws IOException {
            int read = in.read();
            if (read != -1) {
                m_mac.update((byte) read);
            }
            return read;
        }

        @Override
        public int read(byte[] b, int off, int len) throws IOException {
            int read = in.read(b, off, len);
            if (read != -1) {
                m_mac.update(b, off, read);
            }
            return read;
        }

        @Override
        public long skip(long n) throws IOException {
            throw new UnsupportedOperationException();
        }
    }

    public static class HmacOutputStream extends FilterOutputStream {
        private final Mac m_mac;

        public HmacOutputStream(OutputStream out, SecretKey key) throws GeneralSecurityException {
            super(out);

            m_mac = Mac.getInstance(key.getAlgorithm());
            m_mac.init(key);
        }

        public byte[] getMAC() throws IOException {
            return m_mac.doFinal();
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            m_mac.update(b, off, len);
        }

        @Override
        public void write(int b) throws IOException {
            out.write(b);
            m_mac.update((byte) b);
        }
    }

    static Account readAccount(DataInputStream dis) throws IOException {
        String accessToken = null;

        String id = dis.readUTF();
        State state = State.valueOf(dis.readUTF());
        boolean hasAccessToken = dis.readBoolean();
        if (hasAccessToken) {
            accessToken = dis.readUTF();
        }
        boolean locked = dis.readBoolean();
        int entryCount = dis.readInt();

        Map<String, String> creds = new HashMap<>();
        for (int i = 0; i < entryCount; i++) {
            creds.put(dis.readUTF(), dis.readUTF());
        }

        if (creds.size() != entryCount) {
            throw new IOException("Invalid accounts file: unable to read all credentials!");
        }

        return new Account(id, creds, state, accessToken, locked);
    }

    static void writeAccount(Account account, DataOutputStream dos) throws IOException {
        dos.writeUTF(account.getId());
        dos.writeUTF(account.getState().name());
        Optional<String> accessToken = account.getAccessToken();
        dos.writeBoolean(accessToken.isPresent());
        if (accessToken.isPresent()) {
            dos.writeUTF(accessToken.get());
        }
        dos.writeBoolean(account.isLocked());

        Map<String, String> creds = account.getCredentials();
        dos.writeInt(creds.size());

        for (Map.Entry<String, String> entry : creds.entrySet()) {
            dos.writeUTF(entry.getKey());
            dos.writeUTF(entry.getValue());
        }
    }
}
