/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest;

import static org.amdatu.security.account.admin.rest.util.ConfigUtil.getBoolean;
import static org.amdatu.security.account.admin.rest.util.ConfigUtil.getString;
import static org.amdatu.security.account.admin.rest.util.ConfigUtil.getURI;

import java.net.URI;
import java.util.Dictionary;
import java.util.Optional;

import org.osgi.service.cm.ConfigurationException;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AccountAdminResourceConfig {

    public static final String KEY_ALLOW_SIGNUP = "allowSignup";
    public static final String KEY_CHANGE_CREDENTIALS_PAGE = "changeCredentialsPage";
    public static final String KEY_CREDENTIALS_CHANGED_PAGE = "credentialsChangedPage";
    public static final String KEY_CREDENTIALS_RESET_REQUESTED_PAGE = "credentialsResetRequestedPage";
    public static final String KEY_ACCOUNT_VERIFIED_PAGE = "accountVerifiedPage";
    public static final String KEY_ACCOUNT_REMOVED_PAGE = "accountRemovedPage";
    public static final String KEY_SIGNUP_SUCCESS_PAGE = "signupSuccessPage";
    public static final String KEY_EMAIL_KEY = "keyEmail";

    private static final boolean DEFAULT_ALLOW_SIGNUP = true;
    private static final String DEFAULT_PAGE = "/account/index.html";
    private static final String DEFAULT_CHANGE_CREDENTIALS_PAGE = DEFAULT_PAGE.concat("#change");
    private static final String DEFAULT_CREDENTIALS_RESET_REQUESTED_PAGE = DEFAULT_PAGE.concat("#reset");
    private static final String DEFAULT_EMAIL_KEY = "email";

    private final boolean m_allowSignup;
    private final URI m_changeCredentialsPage;
    private final URI m_credentialsChangedPage;
    private final URI m_credentialsResetRequestedPage;
    private final URI m_accountVerifiedPage;
    private final URI m_accountRemovedPage;
    private final URI m_signupSuccessPage;
    private final String m_emailKey;

    /**
     * Creates a new {@link AccountAdminResourceConfig} instance.
     */
    public AccountAdminResourceConfig() {
        m_allowSignup = DEFAULT_ALLOW_SIGNUP;
        m_changeCredentialsPage = URI.create(DEFAULT_CHANGE_CREDENTIALS_PAGE);
        m_credentialsChangedPage = URI.create(DEFAULT_PAGE);
        m_credentialsResetRequestedPage = URI.create(DEFAULT_CREDENTIALS_RESET_REQUESTED_PAGE);
        m_accountVerifiedPage = URI.create(DEFAULT_PAGE);
        m_accountRemovedPage = URI.create(DEFAULT_PAGE);
        m_signupSuccessPage = URI.create(DEFAULT_PAGE);
        m_emailKey = DEFAULT_EMAIL_KEY;
    }

    /**
     * Creates a new {@link AccountAdminResourceConfig} instance.
     */
    public AccountAdminResourceConfig(Dictionary<String, ?> config) throws ConfigurationException {
        m_allowSignup = getBoolean(config, KEY_ALLOW_SIGNUP, DEFAULT_ALLOW_SIGNUP);
        m_changeCredentialsPage = getURI(config, KEY_CHANGE_CREDENTIALS_PAGE, DEFAULT_CHANGE_CREDENTIALS_PAGE);
        m_credentialsChangedPage = getURI(config, KEY_CREDENTIALS_CHANGED_PAGE, null);
        m_credentialsResetRequestedPage = getURI(config, KEY_CREDENTIALS_RESET_REQUESTED_PAGE, null);
        m_accountVerifiedPage = getURI(config, KEY_ACCOUNT_VERIFIED_PAGE, null);
        m_accountRemovedPage = getURI(config, KEY_ACCOUNT_VERIFIED_PAGE, null);
        m_signupSuccessPage = getURI(config, KEY_SIGNUP_SUCCESS_PAGE, null);
        m_emailKey = getString(config, KEY_EMAIL_KEY, DEFAULT_EMAIL_KEY);
    }

    /**
     * @return the page that users are redirected to after they've removed their account, can be empty if no redirect is desired.
     */
    public Optional<URI> getAccountRemovedPage() {
        return Optional.ofNullable(m_accountRemovedPage);
    }

    /**
     * @return the page that users are redirected to after they've verified their account, can be empty if no redirect is desired.
     */
    public Optional<URI> getAccountVerifiedPage() {
        return Optional.ofNullable(m_accountVerifiedPage);
    }

    /**
     * @return the page that allows users to change their account credentials, cannot be <code>null</code>.
     */
    public URI getChangeCredentialsPage() {
        return m_changeCredentialsPage;
    }

    /**
     * @return the page that users are redirected to after they've changed their credentials, can be empty if no redirect is desired.
     */
    public Optional<URI> getCredentialsChangedPage() {
        return Optional.ofNullable(m_credentialsChangedPage);
    }

    /**
     * @return the page that users are redirected to after they've requested to reset their credentials, can be <code>null</code> if no redirect is desired.
     */
    public Optional<URI> getCredentialsResetRequestedPage() {
        return Optional.ofNullable(m_credentialsResetRequestedPage);
    }

    /**
     * @return the name of the key used in the credentials to identify the email address of a user.
     */
    public String getEmailKey() {
        return m_emailKey;
    }

    /**
     * @return the page that users are redirected to after they've signed up for an account, can be <code>null</code> if no redirect is desired.
     */
    public Optional<URI> getSignupSuccessPage() {
        return Optional.ofNullable(m_signupSuccessPage);
    }

    /**
     * @return <code>true</code> if users are allowed to signup themselves, <code>false</code> if their accounts are created differently.
     */
    public boolean isAllowSignup() {
        return m_allowSignup;
    }
}
