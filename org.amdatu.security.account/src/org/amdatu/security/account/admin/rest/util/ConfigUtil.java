/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.admin.rest.util;

import java.net.URI;
import java.util.Dictionary;

import org.osgi.service.cm.ConfigurationException;

/**
 * Provides a couple of convenience methods to work with the account admin
 * configuration.
 */
public final class ConfigUtil {

	public static boolean getBoolean(Dictionary<String, ?> dict, String key, boolean dflt)
			throws ConfigurationException {
		String val = getString(dict, key, null);
		if (val == null) {
			return dflt;
		}
		return Boolean.parseBoolean(val);
	}

	public static int getInteger(Dictionary<String, ?> dict, String key, int dflt) throws ConfigurationException {
		String val = getString(dict, key, null);
		if (val == null) {
			return dflt;
		}
		try {
			return Integer.parseInt(val);
		} catch (NumberFormatException e) {
			return dflt;
		}
	}

	public static String getString(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
		Object val = dict.get(key);
		if (val == null) {
			return dflt;
		}
		if (val instanceof String) {
			return ((String) val).trim();
		}
		throw new ConfigurationException(key, "must be a string!");
	}

	public static String[] getStringArray(Dictionary<String, ?> dict, String key, String[] dflt)
			throws ConfigurationException {
		Object val = dict.get(key);
		if (val == null) {
			return dflt;
		}
		if (val instanceof String[]) {
			return (String[]) val;
		}
		if (val instanceof String) {
			return ((String) val).trim().split("\\s*,\\s*");
		}
		throw new ConfigurationException(key, "must be a string!");
	}

	public static URI getURI(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
		String val = getString(dict, key, dflt);
		if (val == null) {
			return null;
		}
		return URI.create(val);
	}
}
