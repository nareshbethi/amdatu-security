/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account;

/**
 * Provides a means of verifying whether an account is considered valid.
 * <p>
 * For example, it might be that accounts must have a minimal password length, or that additional
 * credentials, such as, for example, a phone number, should be provided for all accounts.<br>
 * This interface can be used to implement a security policy for all accounts.
 * </p>
 */
public interface AccountValidator {

    /**
     * Validates whether or not the given account is valid.
     *
     * @param account the account to validate, cannot be <code>null</code>.
     * @throws AccountValidationException in case validation fails.
     */
    void validate(Account account) throws AccountValidationException;

}
