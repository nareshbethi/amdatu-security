/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.amdatu.security.password.hash.PasswordHasher;

/**
 * {@link PasswordHasher} implementation usable for unit tests.
 */
public class TestPasswordHasher implements PasswordHasher {

    private final MessageDigest m_digester;

    /**
     * Creates a new {@link TestPasswordHasher} instance.
     */
    public TestPasswordHasher() {
        try {
            m_digester = MessageDigest.getInstance("SHA-256");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("SHA-256 should be supported by the JCA!");
        }
    }

    @Override
    public String hash(String input) {
        byte[] digest = m_digester.digest(new String(input).getBytes());
        return "$5$".concat(Base64.getEncoder().encodeToString(digest));
    }

    @Override
    public boolean isHashed(String input) {
        if (input == null || input.length() < 3) {
            return false;
        }
        byte[] b = input.getBytes();
        return b[0] == '$' && b[1] == '5' && b[2] == '$';
    }

    @Override
    public boolean verify(String a, String b) {
        if (isHashed(a)) {
            b = isHashed(b) ? b : hash(b);
        }
        return a.equals(b);
    }
}
