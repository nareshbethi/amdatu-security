/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account.backend.file;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.amdatu.security.account.Account;
import org.amdatu.security.account.Account.State;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * Test cases for {@link FileBasedBackend}.
 */
@RunWith(MockitoJUnitRunner.class)
public class FileBasedAccountAdminBackendTest {

    private final SecretKey m_secretKey = new SecretKeySpec(new String("0123456789ABCDEF").getBytes(), "HmacSHA256");

    @Mock
    private BundleContext m_context;
    @Mock
    private LogService m_logService;

    @InjectMocks
    private FileBasedBackend m_backend = new FileBasedBackend(m_secretKey);

    @Rule
    public final ExpectedException m_rule = ExpectedException.none();

    @Test
    public void testReadWriteBackendWithSingleEntryOk() throws Exception {
        File file = createTempFile();

        Account account = new Account("user", singletonMap("password", "secret"), State.ACCOUNT_VERIFICATION_NEEDED, "abcd", true);

        m_backend.create(account);

        m_backend.writeAllAccounts(file);

        m_backend.clear();

        m_backend.readAllAccounts(file);

        assertEquals(1, m_backend.size());
        assertTrue(m_backend.get("user").isPresent());
    }

    @Test
    public void testReadWriteEmptyBackendOk() throws Exception {
        File file = createTempFile();

        m_backend.writeAllAccounts(file);

        m_backend.readAllAccounts(file);

        assertEquals(0, m_backend.size());
    }

    @Test
    public void testReadNonExistingFileOk() throws Exception {
        File file = createTempFile();
        assertTrue(file.delete());

        m_rule.expect(FileNotFoundException.class);

        m_backend.readAllAccounts(file);
    }

    @Test
    public void testReadWriteFilledBackendOk() throws Exception {
        File file = createTempFile();

        Set<Account> origAccounts = new HashSet<>();

        createAccount(origAccounts, "user1", singletonMap("password", "a"), State.NORMAL, null, false);
        createAccount(origAccounts, "user2", singletonMap("password", "b"), State.ACCOUNT_VERIFICATION_NEEDED, "abcd", false);
        createAccount(origAccounts, "user3", singletonMap("password", "c"), State.FORCED_CREDENTIALS_RESET, "defg", false);
        createAccount(origAccounts, "user4", singletonMap("password", "d"), State.VOLUNTARY_CREDENTIALS_RESET, "hijk", false);
        createAccount(origAccounts, "user5", singletonMap("password", "e"), State.NORMAL, null, true);

        m_backend.writeAllAccounts(file);

        // Simulate we're restarting...
        m_backend.clear();

        m_backend.readAllAccounts(file);

        Set<Account> readAccounts = new HashSet<>();

        for (Account origAccount : origAccounts) {
            m_backend.get(origAccount.getId())
                .ifPresent(acc -> readAccounts.add(acc));
        }

        assertEquals(origAccounts.size(), readAccounts.size());
        assertEquals(origAccounts, readAccounts);
    }

    private void createAccount(Set<Account> accounts, String id, Map<String, String> creds, State state, String token, boolean locked) {
        Account account = new Account(id, creds, state, token, locked);
        m_backend.create(account);
        accounts.add(account);
    }

    private File createTempFile() throws IOException {
        File file = File.createTempFile("accounts", ".dat");
        file.deleteOnExit();
        return file;
    }
}
