/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest;

import static org.amdatu.security.tokenprovider.http.TokenUtil.getTokenFromCookie;
import static org.amdatu.web.util.HttpRequestUtil.isSecureRequest;
import static org.osgi.service.log.LogService.LOG_DEBUG;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.security.authentication.authservice.AuthenticationContext;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor;
import org.amdatu.security.tokenprovider.InvalidTokenException;
import org.amdatu.security.tokenprovider.InvalidTokenException.Reason;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.osgi.service.log.LogService;

/**
 * {@link AuthenticationInterceptor} implementation that sets or removes a token cookie from the servlet response.
 */
public class AuthenticationTokenCookieInterceptor implements AuthenticationInterceptor {
    public static final String ATTR_TOKEN_PROPERTIES = "tokenProperties";
    public static final String ATTR_INVALID_TOKEN_REASON = "invalidTokenReason";

    private final AuthenticationRealmConfig m_config;
    // Injected by Felix DM...
    private volatile TokenProvider m_tokenProvider;
    private volatile LogService m_log;

    /**
     * Creates a new {@link AuthenticationTokenCookieInterceptor} instance.
     */
    public AuthenticationTokenCookieInterceptor(AuthenticationRealmConfig config) {
        m_config = config;
    }

    @Override
    public void onAuthenticationEnd(HttpServletRequest request, HttpServletResponse response) {
        request.removeAttribute(ATTR_TOKEN_PROPERTIES);
        request.removeAttribute(ATTR_INVALID_TOKEN_REASON);
    }

    @Override
    public void onAuthenticationStart(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> properties = null;
        Reason reason = null;

        try {
            String token = getTokenFromCookie(request, m_config.getCookieName());
            properties = m_tokenProvider.verifyToken(token);
        }
        catch (IllegalArgumentException e) {
            // Token wasn't supplied at all...
            reason = Reason.UNKNOWN;
        }
        catch (InvalidTokenException e) {
            // Token has expired or wasn't supplied at all...
            reason = e.getReason();
            properties = e.getAttributes().orElse(null);
        }

        request.setAttribute(ATTR_TOKEN_PROPERTIES, properties);
        request.setAttribute(ATTR_INVALID_TOKEN_REASON, reason);
    }

    @Override
    public void onLogin(AuthenticationContext context, LoginReason reason) {
        m_log.log(LOG_DEBUG, "Setting auth cookie for " + context.getUserIdentity() + " on login. Reason: " + reason);

        context.getServletResponse().addCookie(createAuthCookie(m_config, context));
    }

    @Override
    public void onLoginRejected(AuthenticationContext context, RejectReason reason) {
        m_log.log(LOG_DEBUG,
            "Clearing auth cookie for " + context.getUserIdentity() + " on rejected login. Reason: " + reason);

        context.getServletResponse().addCookie(createEmptyCookie(m_config, context));
    }

    @Override
    public void onLogout(AuthenticationContext context, LogoutReason reason) {
        m_log.log(LOG_DEBUG, "Clearing auth cookie for " + context.getUserIdentity() + " on logout. Reason: " + reason);

        context.getServletResponse().addCookie(createEmptyCookie(m_config, context));
    }

    private Cookie createAuthCookie(AuthenticationRealmConfig cfg, AuthenticationContext context) {
        boolean secure = isSecureRequest(context.getServletRequest());

        String token = null;
        Map<String, String> tokenAttrs = context.getUserIdentity();
        if (tokenAttrs != null && !tokenAttrs.isEmpty()) {
            token = m_tokenProvider.generateToken(tokenAttrs);
        }

        Cookie result = new Cookie(cfg.getCookieName(), token);
        cfg.getCookieDomain()
            .ifPresent(result::setDomain);
        result.setMaxAge(cfg.getCookieMaxAge());
        result.setPath(cfg.getCookiePath());
        result.setHttpOnly(true); // this is an authentication credential!
        result.setSecure(secure);
        return result;
    }

    private Cookie createEmptyCookie(AuthenticationRealmConfig cfg, AuthenticationContext context) {
        boolean secure = isSecureRequest(context.getServletRequest());
        int maxAge = 0; // let it be deleted right away!

        Cookie result = new Cookie(cfg.getCookieName(), null);
        cfg.getCookieDomain()
            .ifPresent(result::setDomain);
        result.setPath(cfg.getCookiePath());
        result.setHttpOnly(true); // this is an authentication credential!
        result.setSecure(secure);
        result.setMaxAge(maxAge);
        return result;
    }
}
