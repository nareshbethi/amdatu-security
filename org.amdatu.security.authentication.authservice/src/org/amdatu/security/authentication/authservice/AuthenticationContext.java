/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_INVALID_REQUEST;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Represents the context in which an authentication request takes place, as used for
 * the authentication interceptors.
 *
 * @see AuthenticationInterceptor
 */
@ProviderType
public interface AuthenticationContext {

    /**
     * @return the name of the provider, if known, never <code>null</code>.
     */
    Optional<String> getOptionalProviderType();

    /**
     * @return the name of the provider, never <code>null</code>.
     * @throws AuthenticationException in case no provider type is known.
     */
    default String getProviderType() {
        return getOptionalProviderType()
            .orElseThrow(() -> new AuthenticationException(CODE_INVALID_REQUEST, "No provider type is known!"));
    }

    /**
     * @return the originating servlet request.
     */
    HttpServletRequest getServletRequest();

    /**
     * @return the servlet response for the request.
     */
    HttpServletResponse getServletResponse();

    /**
     * @return the credentials identifying a user.
     */
    Map<String, String> getUserIdentity();

}
