/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc;

import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.KEY_CLIENT_ID;
import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.KEY_CLIENT_SECRET;
import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.KEY_OPEN_ID_CONFIG;
import static org.amdatu.security.authentication.idprovider.oidc.OpenIdConnectProviderConfig.KEY_NAME;
import static org.junit.Assert.assertTrue;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

/**
 * Test cases for {@link OpenIdConnectProviderConfig}.
 */
public class OpenIdConnectProviderConfigTest {

    @Test
    public void testGenerateVerifyStateCodeOk() throws Exception {
        OpenIdConnectProviderConfig cfg = new OpenIdConnectProviderConfig(asMap(KEY_NAME, "myType",
            KEY_CLIENT_ID, "a", KEY_CLIENT_SECRET, "b", KEY_OPEN_ID_CONFIG, new String("{}")));

        String code = cfg.generateStateCode();

        TimeUnit.SECONDS.sleep(1);

        assertTrue("Verify after 1 sec failed!", cfg.verifyStateCode(code));
    }

    private Dictionary<String, ?> asMap(String... values) {
        Dictionary<String, String> result = new Hashtable<>();
        for (int i = 0; i < values.length; i += 2) {
            result.put(values[i], values[i + 1]);
        }
        return result;
    }
}
