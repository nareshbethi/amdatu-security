/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc;

import org.amdatu.security.authentication.idprovider.IdProvider;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * Provides a generic factory that can create OpenID connect-compatible {@link IdProvider}s.
 */
public class Activator extends DependencyActivatorBase {
    private static final String PID = "org.amdatu.security.authentication.idprovider.openidconnect";

    @Override
    public void init(BundleContext context, DependencyManager dm) throws Exception {
        dm.add(createFactoryConfigurationAdapterService(PID, "update", false /* propagate */)
            .setInterface(IdProvider.class.getName(), null)
            .setImplementation(OpenIdConnectProvider.class)
            .add(createServiceDependency().setService(TokenProvider.class).setRequired(true))
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));
    }

}
