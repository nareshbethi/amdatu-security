/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.amdatu.security.authorization.AuthorizationException;
import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.AuthorizationRule;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.BreadAction;
import org.amdatu.security.authorization.EntityProvider;
import org.amdatu.security.authorization.UnauthorizedException;
import org.amdatu.security.authorization.builder.AuthorizationPolicyBuilder;
import org.amdatu.security.authorization.builder.AuthorizationRuleBuilder;
import org.amdatu.security.authorization.builder.EntityDescriptorBuilder;
import org.amdatu.security.authorization.builder.EntitySelectorBuilder;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.junit.Test;
import org.mockito.Mockito;

import junit.framework.TestCase;

public class AuthorizationServiceTest extends TestCase {
    private static final String TEST_RESOURCE = "testResource";
    private static final String TEST_SUBJECT = "testSubject";

    private volatile DependencyManager m_dm;
    private volatile AuthorizationService m_authorizationService;

    @Override
    public void setUp() throws Exception {
        configure(this)
            .add(createServiceDependency().setService(AuthorizationService.class).setRequired(true))
            .apply();
    }

    @Override
    protected void tearDown() throws Exception {
        cleanUp(this);
    }

    @Test
    public void testAuthorizationService() throws InterruptedException {
        AuthorizationRule rule = AuthorizationRuleBuilder.build()
            .withAuthorization((subjectAttrs, resourceAttrs) -> {
                return Optional.of(subjectAttrs.equals(resourceAttrs));
            })
            .done();

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .withSubjectDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_SUBJECT).done())
            .withResourceDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_RESOURCE).done())
            .forActions(EnumSet.allOf(BreadAction.class))
            .withRules(rule)
            .done();

        Map<String, Object> subjectAttributes = new HashMap<>();
        Optional<Map<String, Object>> resourceAttributes = Optional.of(new HashMap<String, Object>());

        // No authorization as the policy is not registered
        assertFalse(m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, resourceAttributes));

        try {
            m_authorizationService.checkAuthorization(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, resourceAttributes);
            fail("expected UnauthorizedException");
        }
        catch (UnauthorizedException e) {
            // Expected
        }

        // Register policy expect authorization
        Component component = registerPolicyProvider(policy);
        assertTrue(m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, resourceAttributes));

        m_authorizationService.checkAuthorization(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, resourceAttributes);

        // Unregister policy authorization should be revoked
        m_dm.remove(component);

        assertFalse(m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, resourceAttributes));

        try {
            m_authorizationService.checkAuthorization(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, resourceAttributes);
            fail("expected UnauthorizedException");
        }
        catch (UnauthorizedException e) {
            // Expected
        }
    }

    @Test
    public void testEntitySelector() throws InterruptedException {
        AuthorizationRule rule = mock(AuthorizationRule.class);
        when(rule.getResourceEntitySelectors())
            .thenReturn(Collections.singletonList(EntitySelectorBuilder.build().withKey("selected").withType("ResolvedEntity").done()));
        when(rule.evaluate(Mockito.anyMap(), Mockito.anyMap())).thenReturn(Optional.empty());

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .withSubjectDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_SUBJECT).done())
            .withResourceDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_RESOURCE).done())
            .forActions(EnumSet.allOf(BreadAction.class))
            .withRules(rule)
            .done();

        Map<String, Object> subjectAttributes = new HashMap<>();
        Map<String, Object> resourceAttributes = new HashMap<String, Object>();
        resourceAttributes.put("test", "ResolvedEntityValue");

        Component component = registerPolicyProvider(policy);
        Component entityProviderComponent = registerEntityProvider("ResolvedEntity", (attrs) -> Optional.of(attrs.get("test")), "test");

        m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, Optional.of(resourceAttributes));

        Map<String, Object> expectedEntities = new HashMap<>(resourceAttributes);
        expectedEntities.put("selected", "ResolvedEntityValue");
        verify(rule).evaluate(subjectAttributes, expectedEntities);

        m_dm.remove(component);
        m_dm.remove(entityProviderComponent);
    }

    @Test
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void testUnsatisfiedEntitySelector() throws InterruptedException {
        AuthorizationRule rule = mock(AuthorizationRule.class);
        when(rule.getResourceEntitySelectors())
            .thenReturn(Collections.singletonList(EntitySelectorBuilder.build().withKey("selected").withType("ResolvedEntity").done()));
        when(rule.evaluate(Mockito.anyMap(), Mockito.anyMap())).thenReturn(Optional.empty());

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .withSubjectDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_SUBJECT).done())
            .withResourceDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_RESOURCE).done())
            .forActions(EnumSet.allOf(BreadAction.class))
            .withRules(rule)
            .done();

        Map<String, Object> subjectAttributes = new HashMap<>();
        Map<String, Object> resourceAttributes = new HashMap<String, Object>();
        resourceAttributes.put("test", "ResolvedEntityValue");

        EntityProvider mockEntityProvider = mock(EntityProvider.class);
        when(mockEntityProvider.getEntityType()).thenReturn("ResolvedEntity");
        when(mockEntityProvider.getRequiredPropertyKeys()).thenReturn(Arrays.asList("test", "missing"));
        Component component = registerPolicyProvider(policy);
        Component entityProviderComponent = registerEntityProvider(mockEntityProvider);

        try {
            m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, Optional.of(resourceAttributes));
            fail("expected AuthorizationException");
        }
        catch (AuthorizationException e) {
            // Expected
        }

        verify(mockEntityProvider, never()).getEntity(Mockito.anyMap());
        verify(rule, never()).evaluate(Mockito.anyMap(), Mockito.anyMap());

        m_dm.remove(component);
        m_dm.remove(entityProviderComponent);
    }

    @Test
    public void testDependentEntitySelectors() throws InterruptedException {
        AuthorizationRule rule = mock(AuthorizationRule.class);
        when(rule.getResourceEntitySelectors())
            .thenReturn(Arrays.asList(
                EntitySelectorBuilder.build().withKey("prov1").withType("ProvidedEntity").done(),
                EntitySelectorBuilder.build().withKey("prov2").withType("OtherProvidedEntity").done()));
        when(rule.evaluate(Mockito.anyMap(), Mockito.anyMap())).thenReturn(Optional.empty());

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .withSubjectDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_SUBJECT).done())
            .withResourceDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_RESOURCE).done())
            .forActions(EnumSet.allOf(BreadAction.class))
            .withRules(rule)
            .done();

        Map<String, Object> subjectAttributes = new HashMap<>();
        Map<String, Object> resourceAttributes = new HashMap<String, Object>();
        resourceAttributes.put("test", "ResolvedEntityValue");

        Component component = registerPolicyProvider(policy);
        Component entityProviderComponent = registerEntityProvider("ProvidedEntity", (attrs) -> Optional.of(attrs.get("test")), "test");
        Component entityProviderComponent2 = registerEntityProvider("OtherProvidedEntity", (attrs) -> Optional.of(attrs.get("test")), "test", "prov1");

        m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, Optional.of(resourceAttributes));

        Map<String, Object> expectedEntities = new HashMap<>(resourceAttributes);
        expectedEntities.put("prov1", "ResolvedEntityValue");
        expectedEntities.put("prov2", "ResolvedEntityValue");
        verify(rule).evaluate(subjectAttributes, expectedEntities);

        m_dm.remove(component);
        m_dm.remove(entityProviderComponent);
        m_dm.remove(entityProviderComponent2);
    }

    @Test
    public void testDependentEntitySelectorsWrongOrder() throws InterruptedException {
        AuthorizationRule rule = mock(AuthorizationRule.class);
        when(rule.getResourceEntitySelectors())
            .thenReturn(Arrays.asList(
                EntitySelectorBuilder.build().withKey("prov2").withType("OtherProvidedEntity").done(),
                EntitySelectorBuilder.build().withKey("prov1").withType("ProvidedEntity").done()));
        when(rule.evaluate(Mockito.anyMap(), Mockito.anyMap())).thenReturn(Optional.empty());

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .withSubjectDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_SUBJECT).done())
            .withResourceDescriptor(EntityDescriptorBuilder.build().withEntityTypes(TEST_RESOURCE).done())
            .forActions(EnumSet.allOf(BreadAction.class))
            .withRules(rule)
            .done();

        Map<String, Object> subjectAttributes = new HashMap<>();
        Map<String, Object> resourceAttributes = new HashMap<String, Object>();
        resourceAttributes.put("test", "ResolvedEntityValue");

        Component component = registerPolicyProvider(policy);
        Component entityProviderComponent = registerEntityProvider("ProvidedEntity", (attrs) -> Optional.of(attrs.get("test")), "test");
        Component entityProviderComponent2 = registerEntityProvider("OtherProvidedEntity", (attrs) -> Optional.of(attrs.get("test")), "test", "prov1");

        try {
            m_authorizationService.isAuthorized(TEST_SUBJECT, subjectAttributes, BreadAction.READ, TEST_RESOURCE, Optional.of(resourceAttributes));
            fail("expected AuthorizationException");
        }
        catch (AuthorizationException e) {
            // Expected
        }

        verify(rule, never()).evaluate(Mockito.anyMap(), Mockito.anyMap());

        m_dm.remove(component);
        m_dm.remove(entityProviderComponent);
        m_dm.remove(entityProviderComponent2);
    }

    private Component registerPolicyProvider(AuthorizationPolicy... policies) {
        Component component = m_dm.createComponent()
            .setInterface(AuthorizationPolicyProvider.class.getName(), null)
            .setImplementation(new AuthorizationPolicyProvider() {

                @Override
                public Set<AuthorizationPolicy> getPolicies() {
                    return new HashSet<>(Arrays.asList(policies));
                }
            });

        m_dm.add(component);
        return component;
    }

    private <T> Component registerEntityProvider(String entityType, Function<Map<String, Object>, Optional<T>> entitySupplier, String... requiredPropertyKeys) {
        return registerEntityProvider(new EntityProvider<T>() {

            @Override
            public String getEntityType() {
                return entityType;
            }

            @Override
            public Collection<String> getRequiredPropertyKeys() {
                return Arrays.asList(requiredPropertyKeys);
            }

            @Override
            public Optional<T> getEntity(Map<String, Object> properties) {
                return entitySupplier.apply(properties);
            }
        });
    }

    private <T> Component registerEntityProvider(EntityProvider<T> entityProvider) {
        Component component = m_dm.createComponent()
            .setInterface(EntityProvider.class.getName(), null)
            .setImplementation(entityProvider);
        m_dm.add(component);
        return component;
    }

}
