/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.AuthorizationRule;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.BreadAction;
import org.amdatu.security.authorization.EntityDescriptor;
import org.amdatu.security.authorization.EntityProvider;
import org.amdatu.security.authorization.EntitySelector;
import org.amdatu.security.authorization.OptionalBiPredicate;
import org.amdatu.security.authorization.builder.AuthorizationPolicyBuilder;
import org.amdatu.security.authorization.builder.AuthorizationRuleBuilder;
import org.amdatu.security.authorization.builder.EntityDescriptorBuilder;
import org.amdatu.security.authorization.builder.EntitySelectorBuilder;

public class MockProtectedPersonService implements AuthorizationPolicyProvider, EntityProvider<MockProtectedPerson> {

    private static final String CONTEXT = "Context";
    private static final String PERSON = "Person";

    private volatile AuthorizationService m_authorizationService;

    @Override
    public Set<AuthorizationPolicy> getPolicies() {
        EntityDescriptor subjectDescriptor = EntityDescriptorBuilder.build().withAttributeQualifiers(MockContextProvider.getUserRoleQualifier("USER")).done();
        EntityDescriptor resourceDescriptor = EntityDescriptorBuilder.buildTypeDescriptor(MockProtectedPerson.class);

        AuthorizationRule parentOfPupilReadingMentorTalk = AuthorizationRuleBuilder.build()
            .withSubjectEntitySelectors(MockContextProvider.getContextSelector())
            .withResourceEntitySelectors(getCurrentPersonSelector())
            .withAuthorization(getAuthorization())
            .done();

        AuthorizationPolicy policy = AuthorizationPolicyBuilder.build()
            .forActions(EnumSet.allOf(BreadAction.class))
            .withSubjectDescriptor(subjectDescriptor)
            .withResourceDescriptor(resourceDescriptor)
            .withRules(parentOfPupilReadingMentorTalk)
            .done();

        return Collections.singleton(policy);
    }

    private EntitySelector getCurrentPersonSelector() {
        return EntitySelectorBuilder.build()
            .withType(MockProtectedPerson.class.getName())
            .withKey(PERSON)
            .done();
    }

    private OptionalBiPredicate<Map<String, Object>, Map<String, Object>> getAuthorization() {
        return (subjectEntities, resourceEntities) -> {
            MockContext context = (MockContext) subjectEntities.get(CONTEXT);
            MockProtectedPerson archive = (MockProtectedPerson) resourceEntities.get(PERSON);
            Person person = context.get(MockContext.USER_ASSOCIATED_PERSON_KEY);
            String userOrganization = context.get(MockContext.USER_ORGANIZATION_KEY);

            if (userOrganization.equals(archive.getDriversLicenseId()) && person.getChildren().contains(archive.getPersonId())) {
                return Optional.of(true);
            }
            return Optional.of(false);
        };
    }

    public MockProtectedPerson getPerson(String personId) {
        Map<String, Object> subjectProperties = new HashMap<String, Object>();
        subjectProperties.put(AuthorizationService.SUBJECT_IDENTIFIER, MockContext.get().getUserUUID());

        Map<String, Object> resourceProperties = new HashMap<String, Object>();
        resourceProperties.put(AuthorizationService.RESOURCE_IDENTIFIER, personId);

        if (m_authorizationService.isAuthorized("user", subjectProperties, BreadAction.READ, MockProtectedPerson.class.getName(), Optional.of(resourceProperties))) {
            return getPersonUnprotected(personId);
        }
        throw new RuntimeException("No access");
    }

    private MockProtectedPerson getPersonUnprotected(String personId) {
        return new MockProtectedPerson(personId);
    }

    @Override
    public String getEntityType() {
        return MockProtectedPerson.class.getName();
    }

    @Override
    public Collection<String> getRequiredPropertyKeys() {
        return Collections.singleton(AuthorizationService.RESOURCE_IDENTIFIER);
    }

    @Override
    public Optional<MockProtectedPerson> getEntity(
        Map<String, Object> properties) {
        return Optional.ofNullable(getPersonUnprotected((String) properties.get(AuthorizationService.RESOURCE_IDENTIFIER)));
    }

}
