:toc: left  
:homepage: https://www.amdatu.org
:source-highlighter: coderay
:icons: font
:imagesdir: images/
:includedir: ../

= Introduction

The Security component offers authentication and authorization features. 
The component makes it easy to implement authentication and authorization for web applications, independent of how users and groups are created and stored.