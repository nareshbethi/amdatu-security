/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.jwt.impl;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenVerificationInterceptor;
import org.amdatu.security.tokenprovider.jwt.JwtKeyProvider;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    private static final String PID = "org.amdatu.security.tokenprovider.jwt";

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        String clazz = JwtKeyProvider.class.getName();

        String[] ifaces = { TokenProvider.class.getName(), clazz, ManagedService.class.getName() };

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, PID);
        props.put("type", "jwt");

        manager.add(createComponent()
            .setInterface(ifaces, props)
            .setImplementation(TokenProviderImpl.class)
            .add(createServiceDependency()
                .setService(TokenVerificationInterceptor.class)
                .setCallbacks("addInterceptor", "removeInterceptor")
                .setRequired(false))
            .add(createServiceDependency().setService(LogService.class)
                .setRequired(false)));

        manager.add(manager
            .createAdapterService(null, "(&(issuer=*)(url=*))", null, null, "set", "set", "unset", null,
                false /* propagate */)
            .setInterface(clazz, null)
            .setImplementation(UrlBasedJwkProvider.class)
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false)));
    }
}
