/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.test.app;

import static java.util.Arrays.asList;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;

/**
 * Mock implementation of {@link AuthenticationService}.
 */
public class MyAuthService implements PrincipalLookupService, LocalIdCredentialsProvider {
    public static final String KNOWN_USER = "known_user@localhost";

    private final Set<String> m_credentialKeys;
    private final Set<String> m_knownUserIds;
    private final Map<String, Map<String, String>> m_accounts;

    /**
     * Creates a new {@link MyAuthService} instance.
     */
    public MyAuthService(String... knownUserIds) {
        m_credentialKeys = new HashSet<>(asList("email", "upn", "sub"));
        m_knownUserIds = new HashSet<>(asList(knownUserIds));
        m_accounts = new HashMap<>();
        Map<String, String> creds = new HashMap<>();
        creds.put("email", KNOWN_USER);
        creds.put("password", "secret");
        m_accounts.put(KNOWN_USER, creds);
    }

    @Override
    public Optional<String> getId(Map<String, String> credentials) {
        return m_accounts.entrySet().stream()
            .filter(e -> credentialsMatch(e.getValue(), credentials))
            .map(e -> e.getKey())
            .findFirst();
    }

    @Override
    public Optional<String> getPrincipal(Map<String, String> credentials) {
        for (String credentialKey : m_credentialKeys) {
            String userName = credentials.get(credentialKey);
            if (m_knownUserIds.contains(userName)) {
                return Optional.of(userName);
            }
        }

        return Optional.empty();
    }

    @Override
    public Optional<Map<String, String>> getPublicCredentials(String localId) {
        return Optional.ofNullable(m_accounts.get(localId));
    }

    private boolean credentialsMatch(Map<String, String> given, Map<String, String> actual) {
        for (String givenKey : given.keySet()) {
            if (!actual.containsKey(givenKey)) {
                return false;
            }
            if (!Objects.equals(actual.get(givenKey), given.get(givenKey))) {
                return false;
            }
        }
        return true;
    }
}
