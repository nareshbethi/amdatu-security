/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test cases for {@link Totp}, based on the test vectors in <a
 * href="https://tools.ietf.org/html/rfc6238#appendix-B">RFC 6238, appendix B</a>.
 */
@RunWith(Parameterized.class)
public class TotpTest {
    private static final String HMAC_SHA1 = "HmacSHA1";
    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String HMAC_SHA512 = "HmacSHA512";

    private static final int DIGITS = 8;

    private static final byte[] HMAC_SHA1_SECRET = "12345678901234567890".getBytes();
    private static final byte[] HMAC_SHA256_SECRET = "12345678901234567890123456789012".getBytes();
    private static final byte[] HMAC_SHA512_SECRET = "1234567890123456789012345678901234567890123456789012345678901234".getBytes();

    private final Totp m_totp;
    private final int m_totpCode;
    private final long m_timestamp;

    /**
     * Creates a new {@link TotpTest} instance.
     */
    public TotpTest(String macAlg, byte[] secret, long timestamp, int totpCode) {
        m_totp = new Totp.Builder().setSecret(secret).setAlgorithm(macAlg).setNumberOfDigits(DIGITS).build();
        m_timestamp = timestamp;
        m_totpCode = totpCode;
    }

    @Parameters
    public static Collection<Object[]> getParameters() {
        return Arrays.asList(new Object[][] {
            { HMAC_SHA1, HMAC_SHA1_SECRET, 59L, 94287082 },
            { HMAC_SHA256, HMAC_SHA256_SECRET, 59L, 46119246 },
            { HMAC_SHA512, HMAC_SHA512_SECRET, 59L, 90693936 },

            { HMAC_SHA1, HMAC_SHA1_SECRET, 1111111109L, 7081804 },
            { HMAC_SHA256, HMAC_SHA256_SECRET, 1111111109L, 68084774 },
            { HMAC_SHA512, HMAC_SHA512_SECRET, 1111111109L, 25091201 },

            { HMAC_SHA1, HMAC_SHA1_SECRET, 1111111111L, 14050471 },
            { HMAC_SHA256, HMAC_SHA256_SECRET, 1111111111L, 67062674 },
            { HMAC_SHA512, HMAC_SHA512_SECRET, 1111111111L, 99943326 },

            { HMAC_SHA1, HMAC_SHA1_SECRET, 1234567890L, 89005924 },
            { HMAC_SHA256, HMAC_SHA256_SECRET, 1234567890L, 91819424 },
            { HMAC_SHA512, HMAC_SHA512_SECRET, 1234567890L, 93441116 },

            { HMAC_SHA1, HMAC_SHA1_SECRET, 2000000000L, 69279037 },
            { HMAC_SHA256, HMAC_SHA256_SECRET, 2000000000L, 90698825 },
            { HMAC_SHA512, HMAC_SHA512_SECRET, 2000000000L, 38618901 },

            { HMAC_SHA1, HMAC_SHA1_SECRET, 20000000000L, 65353130 },
            { HMAC_SHA256, HMAC_SHA256_SECRET, 20000000000L, 77737706 },
            { HMAC_SHA512, HMAC_SHA512_SECRET, 20000000000L, 47863826 },
        });
    }

    @Test
    public void testGenerateOTP() throws Exception {
        assertEquals(m_totpCode, m_totp.generateCode(m_timestamp));
    }

    @Test
    public void testValidateOTP() throws Exception {
        assertTrue(m_totp.validateCode(m_totpCode, m_timestamp));
    }

}
