/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.hash.pbkdf2;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.security.password.hash.PasswordHasher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator implements BundleActivator {
    private static final String PID = "org.amdatu.security.password";

    @Override
    public void start(BundleContext context) throws Exception {
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, PID);

        context.registerService(PasswordHasher.class, new Pbkdf2Hasher(), props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        // Nop
    }

}
