/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.local.impl.util;

import java.net.URI;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

import org.osgi.service.cm.ConfigurationException;

/**
 * Provides various utility methods for use with a configuration {@link Dictionary}.
 */
public final class ConfigUtils {

    public static int getInteger(Dictionary<String, ?> dict, String key, int dflt) throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null) {
            return dflt;
        }
        try {
            return Integer.parseInt(val);
        }
        catch (NumberFormatException e) {
            return dflt;
        }
    }

    public static Long getLong(Map<String, String[]> params, String key) {
        String[] val = params.get(key);
        if (val == null || val.length < 1) {
            return null;
        }
        try {
            return Long.parseLong(val[0], 16);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

    public static String[] getMandatoryStringArray(Dictionary<String, ?> dict, String key)
        throws ConfigurationException {
        String[] result = getStringArray(dict, key, null);
        if (result == null || result.length == 0) {
            throw new ConfigurationException(key, "not supplied!");
        }
        return result;
    }

    public static String getMandatoryString(Dictionary<String, ?> dict, String key) throws ConfigurationException {
        String result = getString(dict, key, null);
        if (result == null || "".equals(result)) {
            throw new ConfigurationException(key, "not supplied!");
        }
        return result;
    }

    public static URI getMandatoryURI(Dictionary<String, ?> dict, String key) throws ConfigurationException {
        URI val = getURI(dict, key);
        if (val == null) {
            throw new ConfigurationException(key, "not supplied!");
        }
        return val;
    }

    public static String getString(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
        Object val = dict.get(key);
        if (val == null) {
            return dflt;
        }
        if (val instanceof String) {
            return ((String) val).trim();
        }
        throw new ConfigurationException(key, "must be a string!");
    }

    public static String getString(Map<String, String[]> params, String key) {
        String[] val = params.get(key);
        if (val == null || val.length < 1) {
            return null;
        }
        return val[0];
    }

    public static String[] getStringArray(Dictionary<String, ?> dict, String key, String[] dflt)
        throws ConfigurationException {
        Object val = dict.get(key);
        if (val == null) {
            return dflt;
        }
        if (val instanceof String[]) {
            return (String[]) val;
        }
        if (val instanceof String) {
            return ((String) val).trim().split("\\s*,\\s*");
        }
        throw new ConfigurationException(key, "must be a string!");
    }

    public static URI getURI(Dictionary<String, ?> dict, String key) throws ConfigurationException {
        String val = getString(dict, key, null);
        if (val == null) {
            return null;
        }
        return URI.create(val);
    }

    public static Dictionary<String, ?> asDictionary(Object... args) {
        Dictionary<String, Object> result = new Hashtable<>();
        for (int i = 0; i < args.length; i += 2) {
            result.put(args[i].toString(), args[i + 1]);
        }
        return result;
    }
}
