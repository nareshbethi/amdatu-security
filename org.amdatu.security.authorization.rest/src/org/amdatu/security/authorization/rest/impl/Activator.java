/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.impl;

import java.util.Properties;

import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) {

        Properties interceptorProps = new Properties();
        interceptorProps.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION, true);
        interceptorProps.put(JaxrsWhiteboardConstants.JAX_RS_NAME, "amdatu-authorization");
        interceptorProps.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, "(amdatu-authorization=true)");
        manager.add(createComponent().setInterface(JaxRsRequestInterceptor.class.getName(), interceptorProps)
            .setImplementation(AuthorizationRequestInterceptor.class)
            .add(createServiceDependency().setService(AuthorizationService.class).setRequired(true)));

        manager.add(createComponent()
            .setInterface(AuthorizationPolicyProvider.class.getName(), null)
            .setImplementation(RbacAnnotationAuthorizationPolicyProvider.class)
            .add(createServiceDependency().setService("(objectClass=*)").setCallbacks("serviceAdded", "serviceRemoved"))
            );
    }

}
