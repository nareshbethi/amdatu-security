/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.openid;

import static org.amdatu.security.authentication.idprovider.openid.util.ConfigUtil.getMandatoryString;

import java.net.URI;
import java.util.Dictionary;
import java.util.Map;

import org.openid4java.association.AssociationException;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.Discovery;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.yadis.YadisResolver;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.MessageException;
import org.openid4java.message.ParameterList;
import org.openid4java.server.RealmVerifierFactory;
import org.openid4java.util.HttpFetcherFactory;
import org.osgi.service.cm.ConfigurationException;

/**
 * Provides the configurable parts for the OpenID provider.
 */
public class OpenIdProviderConfig {
    /**
     * MANDATORY. The identifying name of the OpenID provider, used to distinguish between multiple OpenID providers.
     */
    private static final String KEY_NAME = "name";
    /**
     * MANDATORY. The OpenID endpoint used to discover the OpenID provider settings.
     */
    private static final String KEY_ENDPOINT = "endpoint";

    public static final String TYPE_OPENID = "openid";

    private final String m_name;
    private final String m_endpoint;

    private final ConsumerManager m_manager;

    // Locally managed
    private DiscoveryInformation m_discoveryInfo;

    /**
     * Creates a new {@link OpenIdProviderConfig} instance.
     */
    public OpenIdProviderConfig(Dictionary<String, ?> config) throws ConfigurationException {
        m_name = getMandatoryString(config, KEY_NAME);
        m_endpoint = getMandatoryString(config, KEY_ENDPOINT);

        m_manager = new ConsumerManager(
        		new RealmVerifierFactory(new YadisResolver(new HttpFetcherFactory())), // use the default HttpFetcherFactory
        		new Discovery(), // uses HttpCache internally
        		new HttpFetcherFactory(() -> new HttpCacheRetryingPost()) // use a CUSTOM HttFetcherFactory, that returns HttpCacheRetryingPost as implementation of HttpFetcher
        );

        // Do not use associations, and try to use stateless authentication...
        m_manager.setMaxAssocAttempts(0);
    }

    public String createAuthURI(URI callbackURI) throws DiscoveryException, MessageException, ConsumerException {
        DiscoveryInformation discovered = getDiscoveryInfo();

        AuthRequest authReq = m_manager.authenticate(discovered, callbackURI.toASCIIString());

        return authReq.getDestinationUrl(true /* httpGet */);
    }

    public VerificationResult verifyResult(URI callbackURI, Map<String, String[]> responseMap)
        throws DiscoveryException, MessageException, AssociationException {
        DiscoveryInformation discovered = getDiscoveryInfo();

        ParameterList responseList = new ParameterList(responseMap);

        return m_manager.verify(callbackURI.toASCIIString(), responseList, discovered);
    }

    /**
     * @return the OpenID discovery endpoint, never <code>null</code>.
     */
    public String getEndpoint() {
        return m_endpoint;
    }

    /**
     * @return the name of OpenID provider, like "yahoo" or "steam".
     */
    public String getName() {
        return m_name;
    }

    private DiscoveryInformation getDiscoveryInfo() throws DiscoveryException {
        if (m_discoveryInfo == null) {
            m_discoveryInfo = m_manager.associate(m_manager.discover(m_endpoint));
        }
        return m_discoveryInfo;
    }

}
