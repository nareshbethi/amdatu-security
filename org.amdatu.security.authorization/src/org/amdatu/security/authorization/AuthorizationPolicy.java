/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.Set;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Represents an authorization policy for a specific set of subjects and objects.
 */
@ConsumerType
public interface AuthorizationPolicy {

    /**
     * @return the set of actions this policy applies to
     */
    Set<Action> getActions();

    /**
     * @return a descriptor for the subject(s) this policy applies to
     */
    public EntityDescriptor getSubjectDescriptor();

    /**
     * @return a descriptor for the resource(s) this policy applies to
     */
    public EntityDescriptor getResourceDescriptor();

    /**
     * @return the set of rules enforced by this policy
     */
    Set<AuthorizationRule> getRules();
}
