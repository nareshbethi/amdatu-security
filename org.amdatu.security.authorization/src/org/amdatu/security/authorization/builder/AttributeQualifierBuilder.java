/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.builder;

import java.util.Map;
import java.util.function.Predicate;

import org.amdatu.security.authorization.AttributeQualifier;
import org.amdatu.security.authorization.EntitySelector;

public class AttributeQualifierBuilder extends BuilderBase<AttributeQualifier> {

    public static AttributeQualifierBuilder build() {
        return new AttributeQualifierBuilder();
    }

    private AttributeQualifierBuilder() {
        super(() -> new AttributeQualifierImpl());
    }

    public AttributeQualifierBuilder withEntitySelector(EntitySelector entitySelector) {
        return with((AttributeQualifierImpl qualifier) -> qualifier.entitySelector = entitySelector);
    }

    public AttributeQualifierBuilder withQualification(Predicate<Map<String, Object>> qualification) {
        return with((AttributeQualifierImpl qualifier) -> qualifier.qualification = qualification);
    }

    private static class AttributeQualifierImpl implements AttributeQualifier {
        private EntitySelector entitySelector = null;
        private Predicate<Map<String, Object>> qualification = (e) -> false;

        @Override
        public EntitySelector getEntitySelector() {
            return entitySelector;
        }

        @Override
        public boolean isQualified(Map<String, Object> properties) {
            return qualification.test(properties);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((entitySelector == null) ? 0 : entitySelector.hashCode());
            result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            AttributeQualifierImpl other = (AttributeQualifierImpl) obj;
            if (entitySelector == null) {
                if (other.entitySelector != null) {
                    return false;
                }
            }
            else if (!entitySelector.equals(other.entitySelector)) {
                return false;
            }
            if (qualification == null) {
                if (other.qualification != null) {
                    return false;
                }
            }
            else if (!qualification.equals(other.qualification)) {
                return false;
            }
            return true;
        }
    }
}
