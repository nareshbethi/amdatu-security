/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.authorization.AuthorizationRule;
import org.amdatu.security.authorization.EntitySelector;
import org.amdatu.security.authorization.OptionalBiPredicate;

public final class AuthorizationRuleBuilder extends BuilderBase<AuthorizationRule> {

    public static AuthorizationRuleBuilder build() {
        return new AuthorizationRuleBuilder();
    }

    private AuthorizationRuleBuilder() {
        super(() -> new AuthorizationRuleImpl());
    }

    public AuthorizationRuleBuilder withSubjectEntitySelectors(EntitySelector... subjectEntitySelectors) {
        return with((AuthorizationRuleImpl rule) -> Collections.addAll(rule.subjectEntitySelectors, subjectEntitySelectors));
    }

    public AuthorizationRuleBuilder withResourceEntitySelectors(EntitySelector... resourceEntitySelectors) {
        return with((AuthorizationRuleImpl rule) -> Collections.addAll(rule.resourceEntitySelectors, resourceEntitySelectors));
    }

    public AuthorizationRuleBuilder withAuthorization(OptionalBiPredicate<Map<String, Object>, Map<String, Object>> authorization) {
        return with((AuthorizationRuleImpl rule) -> rule.authorization = authorization);
    }

    @Override
    protected void finalizeInstance() {
        with((AuthorizationRuleImpl rule) -> rule.subjectEntitySelectors = Collections.unmodifiableList(rule.subjectEntitySelectors));
        with((AuthorizationRuleImpl rule) -> rule.resourceEntitySelectors = Collections.unmodifiableList(rule.resourceEntitySelectors));
    }

    private static final class AuthorizationRuleImpl implements AuthorizationRule {
        private List<EntitySelector> subjectEntitySelectors = new ArrayList<>();
        private List<EntitySelector> resourceEntitySelectors = new ArrayList<>();
        private OptionalBiPredicate<Map<String, Object>, Map<String, Object>> authorization = (s, r) -> Optional.empty();

        @Override
        public List<EntitySelector> getSubjectEntitySelectors() {
            return subjectEntitySelectors;
        }

        @Override
        public List<EntitySelector> getResourceEntitySelectors() {
            return resourceEntitySelectors;
        }

        @Override
        public Optional<Boolean> evaluate(Map<String, Object> subjectEntities, Map<String, Object> resourceEntities) {
            return authorization.test(subjectEntities, resourceEntities);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((authorization == null) ? 0 : authorization.hashCode());
            result = prime * result + ((resourceEntitySelectors == null) ? 0 : resourceEntitySelectors.hashCode());
            result = prime * result + ((subjectEntitySelectors == null) ? 0 : subjectEntitySelectors.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            AuthorizationRuleImpl other = (AuthorizationRuleImpl) obj;
            if (authorization == null) {
                if (other.authorization != null) {
                    return false;
                }
            }
            else if (!authorization.equals(other.authorization)) {
                return false;
            }
            if (resourceEntitySelectors == null) {
                if (other.resourceEntitySelectors != null) {
                    return false;
                }
            }
            else if (!resourceEntitySelectors.equals(other.resourceEntitySelectors)) {
                return false;
            }
            if (subjectEntitySelectors == null) {
                if (other.subjectEntitySelectors != null) {
                    return false;
                }
            }
            else if (!subjectEntitySelectors.equals(other.subjectEntitySelectors)) {
                return false;
            }
            return true;
        }
    }
}
