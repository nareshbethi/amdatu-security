/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.builder;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Base class for Builders.
 *
 * @param <T>
 *            the type of object constructed by the builder
 */
public abstract class BuilderBase<T> {

    private boolean done;

    private T instanceUnderConstruction;

    private final Supplier<? extends T> instanceSupplier;

    /**
     * Construct a new Builder
     *
     * @param instanceSupplier
     *            supplier for instances of the object under construction. Note that the supplier is expected to return
     *            a new instance each time. The type of objects returned by the supplier is expected to match the type
     *            accepted by the setters that are passed to the {@link #with(Consumer)} method
     *
     * @see #with(Consumer)
     */
    protected BuilderBase(Supplier<? extends T> instanceSupplier) {
        this.instanceSupplier = instanceSupplier;
        reset();
    }

    /**
     * Reset the builder to prepare it for construction of a new instance.
     */
    public final void reset() {
        this.done = false;
        this.instanceUnderConstruction = instanceSupplier.get();
    }

    /**
     * Get the instance constructed by this builder. Once this method is called, further attempts to modify the state of
     * the instance through this builder will result in an {@link IllegalStateException}.
     *
     * @return the instance constructed by this builder
     */
    public final T done() {
        checkNotDone();
        finalizeInstance();
        done = true;
        return instanceUnderConstruction;
    }

    /**
     * Additional actions to perform just before the instance under construction is returned to the client.
     *
     * The default implementation does nothing. Subclasses should override this method if they want to perform
     * additional initialization of the instance. This could also include sanity checks on the state of the instance, if
     * needed. In that case, it is recommended that an IllegalStateException be thrown.
     */
    protected void finalizeInstance() {
        // do nothing by default
    }

    /**
     * Allow the specified setter access to the instance under construction by this builder, usually to modify it.
     *
     * @param setter
     *            a consumer that accepts instances of the type that is under construction by this builder. The type of
     *            objects accepted by the specified setter is expected to match the type returned by the supplier passed
     *            to the constructor of this builder.
     * @return this builder
     *
     * @see #BuilderBase(Supplier)
     */
    @SuppressWarnings("unchecked")
    protected final <B extends BuilderBase<T>, U extends T> B with(final Consumer<U> setter) {
        checkNotDone();
        setter.accept((U) instanceUnderConstruction);
        return (B) this;
    }

    private final void checkNotDone() {
        if (done) {
            throw new IllegalStateException("Builder is already done.");
        }
    }
}
