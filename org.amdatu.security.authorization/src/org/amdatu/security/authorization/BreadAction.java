/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

/**
 * Typical data manipulation actions according to the BREAD model.
 */
public enum BreadAction implements Action {
    /**
     * Allows access to the existence/summary of a protected item, for example in overviews and search results.
     */
    BROWSE,
    /**
     * Allows read-only access to the protected item.
     */
    READ,
    /**
     * Allows modification of a protected item.
     */
    EDIT,
    /**
     * Permits the addition of new items.
     */
    ADD,
    /**
     * Allows removing a protected item.
     */
    DELETE;
}
