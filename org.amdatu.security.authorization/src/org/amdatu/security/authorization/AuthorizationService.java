/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Verifies whether or not a specific subject (e.g. a user) is allowed to perform a specific action on a specific resource.
 */
@ProviderType
public interface AuthorizationService {

	public static final String SUBJECT_IDENTIFIER = "subject.id";

	public static final String RESOURCE_IDENTIFIER = "resource.id";

    /**
     * Does the specified subject have authorization to access the specified object?
     *
     * @param subjectType
     * 			  the type of the subject
     * @param subjectAttributes
     *            set of attributes identifying the subject, amongst the identifier of the subject (most commonly a user) requesting access
     * @param action
     *            the type of action requested
     * @param resourceType
     *            the type of resource to which access is requested
     * @param resourceAttributes
     *            set of attributes identifying the resource
     * @return true if the subject is allowed to access the object, false otherwise
     */
    boolean isAuthorized(String subjectType, Map<String, Object> subjectAttributes, Action action, String resourceType, Optional<Map<String, Object>> resourceAttributes);

    /**
     * Does the specified subject have authorization to access the specified object?
     *
     * @param subjectType
     *            the type of the subject
     * @param subjectAttributes
     *            set of attributes identifying the subject, amongst the identifier of the subject (most commonly a user) requesting access
     * @param action
     *            the type of action requested
     * @param resourceType
     *            the type of resource to which access is requested
     * @param resourceAttributes
     *            set of attributes identifying the resource
     * @throws UnauthorizedException when the specified subject doesn't have authorization to access the specified object
     */
    void checkAuthorization(String subjectType, Map<String, Object> subjectAttributes, Action action,
        String resourceType, Optional<Map<String, Object>> resourceAttributes) throws UnauthorizedException;

    /**
     * Does the specified subject have authorization to access the specified object?
     *
     * @param subjectType
     * 			  the type of the subject
     * @param subjectIdentifier
     *            shorthand for passing in subject attributes with just the subject identifier with SUBJECT_IDENTIFIER as its key.
     * @param action
     *            the type of action requested
     * @param resourceType
     *            the type of resource to which access is requested
     * @param resourceIdentifier
     *            shorthand for passing in subject attributes with just the resource identifier with RESOURCE_IDENTIFIER as its key.
     * @return true if the subject is allowed to access the object, false otherwise
     */
    boolean isAuthorized(String subjectType, String subjectIdentifier, Action action, String resourceType, Optional<String> resourceIdentifier);

    /**
     * Does the specified subject have authorization to access the specified object?
     *
     * @param subjectType
     *            the type of the subject
     * @param subjectIdentifier
     *            shorthand for passing in subject attributes with just the subject identifier with SUBJECT_IDENTIFIER as its key.
     * @param action
     *            the type of action requested
     * @param resourceType
     *            the type of resource to which access is requested
     * @param resourceIdentifier
     *            shorthand for passing in subject attributes with just the resource identifier with RESOURCE_IDENTIFIER as its key.
     * @throws UnauthorizedException when the specified subject doesn't have authorization to access the specified object
     */
    void checkAuthorization(String subjectType, String subjectIdentifier, Action action, String resourceType,
        Optional<String> resourceIdentifier) throws UnauthorizedException;

    /**
     * Determine the set of access types the specified user has to the specified (collection of) object(s)
     *
     * @param subjectType
     * 			  the type of the subject
     * @param subjectAttributes
     *            set of attributes identifying the subject, amongst the identifier of the subject (most commonly a user) requesting access
     * @param resourceType
     *            the type of object to which access is requested
     * @param resourceIdentifier
     *            the identifier of the object to which access is requested (optional)
     * @param resourceAttributes
     *            set of attributes identifying the resource
     * @return the set of access types the user has, or an empty set if the user does not have access
     */
    Set<Action> getAuthorizedActions(String subjectType, Map<String, Object> subjectAttributes, String resourceType, Optional<Map<String, Object>> resourceAttributes);

    /**
     * Determine the set of access types the specified user has to the specified (collection of) object(s)
     *
     * @param subjectType
     * 			  the type of the subject
     * @param subjectIdentifier
     *            shorthand for passing in subject attributes with just the subject identifier with SUBJECT_IDENTIFIER as its key.
     * @param resourceType
     *            the type of object to which access is requested
     * @param resourceIdentifier
     *            the identifier of the object to which access is requested (optional)
     * @param resourceIdentifier
     *            shorthand for passing in subject attributes with just the resource identifier with RESOURCE_IDENTIFIER as its key.
     * @return the set of access types the user has, or an empty set if the user does not have access
     */
    Set<Action> getAuthorizedActions(String subjectType, String subjectIdentifier, String resourceType, Optional<String> resourceIdentifier);
}
