/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.Collections;
import java.util.Set;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Represents a membership query for entities. It describes all entities that match all specified attribute qualifiers.
 */
@ConsumerType
public interface EntityDescriptor {

    public static final EntityDescriptor UNDEFINED = new EntityDescriptor() {
        @Override
        public Set<AttributeQualifier> getAttributeQualifiers() {
            return Collections.emptySet();
        }
        public Set<String> getEntityTypes() {
        	return Collections.emptySet();
        };
    };

    public Set<String> getEntityTypes();

    /**
     * @return the {@link AttributeQualifier}s that describe the entity attributes to match
     */
    public Set<AttributeQualifier> getAttributeQualifiers();
}
