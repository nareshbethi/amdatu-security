/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.Optional;
import java.util.function.BiPredicate;

/**
 * Represents a predicate (boolean-valued function) of two arguments, that may have an undetermined result. This is the
 * {@link Optional} specialization of {@link BiPredicate}.
 *
 * <p>
 * This is a functional interface whose functional method is {@link #test(Object, Object)}.
 *
 * @param <T>
 *            the type of the first argument to the predicate
 * @param <U>
 *            the type of the second argument the predicate
 *
 * @see Optional
 * @see BiPredicate
 */
@FunctionalInterface
public interface OptionalBiPredicate<T, U> {

    /**
     * Evaluates this predicate on the given arguments.
     *
     * @param t
     *            the first input argument
     * @param u
     *            the second input argument
     * @return an {@link Optional} Boolean value, that contains a TRUE value if the input arguments match the predicate,
     *         a FALSE value if the input arguments don't match the predicate or no value (i.e. its
     *         {@link Optional#isPresent()} method will return false) if the result of the match is undetermined for the
     *         input arguments
     */
    Optional<Boolean> test(T t, U u);

    /**
     * Returns a composed predicate that represents a short-circuiting logical AND of this predicate and another. When
     * evaluating the composed predicate, if this predicate is {@code false}, then the {@code other} predicate is not
     * evaluated.
     *
     * <p>
     * If this predicate and the other predicate both result in an empty Optional, the predicate returned by this method
     * will result in an empty Optional as well. If either this predicate or the other predicate has a non-empty result,
     * that result will also be the result of the predicate returned by this method.
     *
     * <p>
     * Any exceptions thrown during evaluation of either predicate are relayed to the caller; if evaluation of this
     * predicate throws an exception, the {@code other} predicate will not be evaluated.
     *
     * @param other
     *            a predicate that will be logically-ANDed with this predicate
     * @return a composed predicate that represents the short-circuiting logical AND of this predicate and the
     *         {@code other} predicate
     * @throws NullPointerException
     *             if other is null
     */
    default OptionalBiPredicate<T, U> and(OptionalBiPredicate<? super T, ? super U> other) {
        // @formatter:off
        return (T t, U u) -> {
           Optional<Boolean> result = this.test(t, u)
               .flatMap(b -> b ?
                   Optional.of(other.test(t, u).orElse(true)) :
                   Optional.of(false));
           if(result.isPresent()) {
               return result;
           }
           return other.test(t, u);
        };

        // @formatter:on
    }

    /**
     * Returns a composed predicate that represents a short-circuiting logical OR of this predicate and another. When
     * evaluating the composed predicate, if this predicate is {@code true}, then the {@code other} predicate is not
     * evaluated.
     *
     * <p>
     * If this predicate and the other predicate both result in an empty Optional, the predicate returned by this method
     * will result in an empty Optional as well. If either this predicate or the other predicate has a non-empty result,
     * that result will also be the result of the predicate returned by this method.
     *
     * <p>
     * Any exceptions thrown during evaluation of either predicate are relayed to the caller; if evaluation of this
     * predicate throws an exception, the {@code other} predicate will not be evaluated.
     *
     * @param other
     *            a predicate that will be logically-ORed with this predicate
     * @return a composed predicate that represents the short-circuiting logical OR of this predicate and the
     *         {@code other} predicate
     * @throws NullPointerException
     *             if other is null
     */
    default OptionalBiPredicate<T, U> or(OptionalBiPredicate<? super T, ? super U> other) {
        // @formatter:off
        return (T t, U u) -> {
           Optional<Boolean> result = this.test(t, u)
               .flatMap(b -> b ?
                   Optional.of(true) :
                   Optional.of(other.test(t, u).orElse(false)));
           if(result.isPresent()) {
               return result;
           }
           return other.test(t, u);
        };
        // @formatter:on
    }

    /**
     * Returns a predicate that represents the logical negation of this predicate. If this predicate returns an empty
     * result, so will the predicate returned by this method
     *
     * @return a predicate that represents the logical negation of this predicate
     */
    default OptionalBiPredicate<T, U> negate() {
        return (T t, U u) -> this.test(t, u).map(b -> !b);
    }

    /**
     * Returns a {@link BiPredicate} that performs the same test as this predicate and returns false when this predicate
     * would return an empty Optional.
     *
     * @return a BiPredicate that performs the same test as this predicate and returns false when this predicate would
     *         return an empty Optional
     */
    default BiPredicate<T, U> asBiPredicateUndeterminedIsFalse() {
        return (T t, U u) -> this.test(t, u).orElse(false);
    }

    /**
     * Returns a {@link BiPredicate} that performs the same test as this predicate and returns true when this predicate
     * would return an empty Optional.
     *
     * @return a BiPredicate that performs the same test as this predicate and returns true when this predicate would
     *         return an empty Optional
     */
    default BiPredicate<T, U> asBiPredicateUndeterminedIsTrue() {
        return (T t, U u) -> this.test(t, u).orElse(true);
    }

    /**
     * Returns an OptionalBiPredicate that performs the same test as the specified BiPredicate.
     *
     * @param biPredicate
     *            the BiPredicate
     * @return an OptionalBiPredicate that performs the same test as the specified BiPredicate
     */
    static <T, U> OptionalBiPredicate<T, U> of(BiPredicate<T, U> biPredicate) {
        return (T t, U u) -> Optional.of(biPredicate.test(t, u));
    }
}
